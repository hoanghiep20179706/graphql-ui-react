import { ApolloProvider } from "@apollo/client";
import "./App.css";

import { ApolloClient, InMemoryCache } from "@apollo/client";
import ListAll from "./Components/ListAll";

const client = new ApolloClient({
  uri: "http://103.107.183.254:4000/graphql",
  cache: new InMemoryCache(),
});

// client
//   .query({
//     query: gql`
//       query GetUsers {
//         users {
//           id
//           name
//           email
//           password
//         }
//       }
//     `,
//   })
//   .then((result) => console.log(JSON.stringify(result)));
function App() {
  return (
    <ApolloProvider client={client}>
      <div id="main">
        <h1>Reading Authentication Information</h1>
        <ListAll></ListAll>
      </div>
    </ApolloProvider>
  );
}

export default App;
