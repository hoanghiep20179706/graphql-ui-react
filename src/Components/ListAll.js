import React from "react";
import { LIST_USER_ALL } from "../constants/query";
import { LIST_BALANCE_ALL } from "../constants/query";
import UserList from "./UserList";
import CommentList from "./CommentList";
import { useQuery } from "@apollo/client";

function ListAll(props) {
  // const returnValue = useQuery(gql`
  //   {
  //     users {
  //       id
  //       name
  //       email
  //       password
  //     }
  //   }
  // `);
  // const { error, loading, data } = useQuery(gql`
  //   query GetUsers {
  //     users {
  //       id
  //       name
  //       email
  //       password
  //     }
  //   }
  // `);

  const queryUserResult = useQuery(LIST_USER_ALL);
  const queryBalanceResult = useQuery(LIST_BALANCE_ALL);
  if (queryBalanceResult.loading) return <p>Loading books...</p>;
  if (queryBalanceResult.error) return <p>Error When Loading books:(</p>;

  if (queryUserResult.loading) return <p>Loading users...</p>;
  if (queryUserResult.error) return <p>Error When Loading users:(</p>;

  // useEffect(() => {
  //   console.log(JSON.stringify(data), "rearafdsfdsfdfafd");
  // }, [data]);

  const displayUsers = () => {
    if (!queryUserResult.data || !queryUserResult.data.users) {
      return;
    }
    return (
      <UserList id="user-list" users={queryUserResult.data.users}></UserList>
    );
  };

  // useEffect(() => {
  //   console.log(JSON.stringify(data), "rearafdsfdsfdfafd");
  // }, [data]);

  const displayBalances = () => {
    if (!queryBalanceResult.data || !queryBalanceResult.data.posts) {
      return;
    }
    return queryBalanceResult.data.posts.map(
      ({ id, name, content, userId, comments }) => (
        <li key={id}>
          <div>
            <p>
              {id} - {name}: {content} : {userId}
            </p>
            <CommentList comments={comments} id={id}></CommentList>
          </div>
        </li>
      )
    );
  };

  return (
    <div>
      <h2> List Users</h2>
      {displayUsers()}
      <h2> List Posts</h2>
      <ul id="balance-list">{displayBalances()}</ul>
    </div>
  );
}

export default ListAll;
