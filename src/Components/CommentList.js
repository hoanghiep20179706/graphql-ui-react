import React, { Component } from "react";
import Comment from "./Comment";

class CommentList extends Component {
  displayComments = () => {
    const { comments } = this.props;
    return comments.map((comment) => (
      <li key={comment.id}>
        <Comment comment={comment}></Comment>
      </li>
    ));
  };
  render() {
    const { id } = this.props;
    return <ul id={id}>{this.displayComments()}</ul>;
  }
}

export default CommentList;
