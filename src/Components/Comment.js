import React from "react";
import { LIST_USER_ALL } from "../constants/query";
import { LIST_BALANCE_ALL } from "../constants/query";
import { useQuery } from "@apollo/client";
function Comment(props) {
  const { comment } = props;
  const { id, content, userId, postId } = comment;
  const queryUserResult = useQuery(LIST_USER_ALL);
  if (queryUserResult.loading) return <p>Loading users...</p>;
  if (queryUserResult.error) return <p>Error When Loading users:(</p>;
  return (
    <div key={id}>
      <p>
        {id} - {content}: {userId} - {postId}
      </p>
      {/* {this.displayUsers(users)} */}
    </div>
  );
}

export default Comment;
