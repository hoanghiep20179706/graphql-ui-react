import React from "react";

const UserList = ({ id, users }) => {
  const displayUsers = () => {
    return users.map(({ id, name, email, password }) => (
      <li key={id}>
        <div>
          <p>
            {id} : {name}: {email} : {password}
          </p>
        </div>
      </li>
    ));
  };
  return (
    <div>
      <ul id={id}>{displayUsers()}</ul>
    </div>
  );
};

export default UserList;
