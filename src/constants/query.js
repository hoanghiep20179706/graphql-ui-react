import { gql } from "@apollo/client";

export const LIST_USER_ALL = gql`
  query GetUsers {
    users {
      id
      name
      email
      password
    }
  }
`;

export const LIST_BALANCE_ALL = gql`
  query GetPosts {
    posts {
      id
      name
      content
      userId
      comments {
        id
        content
        userId
        postId
      }
    }
  }
`;
